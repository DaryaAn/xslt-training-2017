-- MySQL dump 10.13  Distrib 5.7.20, for Win64 (x86_64)
--
-- Host: localhost    Database: gpt
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gpt_hotel`
--

DROP TABLE IF EXISTS `gpt_hotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `city_id` bigint(20) NOT NULL,
  `country_id` bigint(20) NOT NULL,
  `category_id` enum('hotel','hostel','apartment') NOT NULL,
  `chain_id` bigint(20) DEFAULT NULL,
  `phone` varchar(100) NOT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `check_in` time NOT NULL,
  `check_out` time NOT NULL,
  `email` varchar(250) DEFAULT NULL,
  `zip` varchar(50) NOT NULL,
  `latitude` decimal(10,6) DEFAULT NULL,
  `longitude` decimal(10,6) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `default_image` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `city_id` (`city_id`),
  KEY `country_id` (`country_id`),
  CONSTRAINT `gpt_hotel_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `gpt_location` (`id`),
  CONSTRAINT `gpt_hotel_ibfk_2` FOREIGN KEY (`country_id`) REFERENCES `gpt_location` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel`
--

LOCK TABLES `gpt_hotel` WRITE;
/*!40000 ALTER TABLE `gpt_hotel` DISABLE KEYS */;
INSERT INTO `gpt_hotel` VALUES (1,'NYCNHHH',8,3,'hotel',100,'+1-212-586-7000','+1-212-315-1374','http://www3.hilton.com/en/hotels/new-york/new-york-hilton-midtown-NYCNHHH/index.html','15:00:00','12:00:00',NULL,'10019',40.762500,73.979800,'',NULL,'2017-10-29 20:31:00'),(2,'1598',9,3,'hotel',101,'+1-213-488-3500','+1-213-488-4110','http://www.sheratongrandlosangeles.com/','15:00:00','12:00:00',NULL,'90017',34.047900,118.258400,'',NULL,'2017-10-29 20:52:00'),(3,'miabb',10,3,'hotel',102,'+1-305-374-3900','+1-305-536-6411','http://www.marriott.com/hotels/travel/miabb-miami-marriott-biscayne-bay/','16:00:00','12:00:00',NULL,'33132',25.790900,80.186200,'',NULL,'2017-10-29 21:15:00'),(4,'MSQDTDI',12,5,'hotel',100,'+375-17-309 80 00','+375-17-3098027','http://doubletree3.hilton.com/en/hotels/belarus/doubletree-by-hilton-hotel-minsk-MSQDTDI/index.html','14:00:00','12:00:00','doubletreeminsk.info@hilton.com','220004',53.908200,27.548200,'',NULL,'2017-10-29 21:23:00'),(5,'yh19',12,5,'hotel',NULL,'+375-17-226-90-23','+375-17-226-91-71','http://www.yhotel.by','14:00:00','12:00:00','info@yhotel.by ','220004',53.911300,27.546500,'',NULL,'2017-10-29 21:47:00'),(6,'VHGRS',12,5,'hotel',103,'+375-17-309-50-00','+375 17 309 50 10','http://victoria2.hotel-victoria.by','14:00:00','12:00:00','reservation@hotel-victoria.by ','220035',53.916800,27.541000,'',NULL,'2017-10-29 21:55:00'),(7,'mhpbr',12,5,'hotel',102,'+375-17-309 90 90','+375-17-309 90 91','http://www.marriott.com/hotels/travel/mhpbr-renaissance-minsk-hotel/','14:00:00','12:00:00','renaissanceminsk@marriott.com ','220036',53.891800,27.529100,'',NULL,'2017-10-29 22:06:00'),(8,'hsttr',12,5,'hostel',NULL,'+375 29 311 27 83',NULL,'http://hostel-traveler.by/','14:00:00','12:00:00','hosteltravelerbelarus@gmail.com ','220029',53.891900,27.529200,'\0',NULL,'2017-10-29 22:17:00'),(9,'hnemgr',13,5,'hotel',NULL,'+375-152-791700','+375-152-791746','http://www.hotel-neman.by','15:00:00','12:00:00','info@hotel-neman.by','230025',53.676600,23.830100,'',NULL,'2017-10-29 22:25:00'),(10,'WAWHIHI',14,6,'hotel',100,'+48-22-356-5555','+48-22-356 5556','http://www3.hilton.com/en/hotels/poland/hilton-warsaw-hotel-and-convention-centre-WAWHIHI/index.html','16:00:00','12:00:00','info.warsaw@hilton.com','00-844',52.233500,20.986100,'',NULL,'2017-10-29 22:35:00'),(11,'3374',15,6,'hotel',104,'+48-71-3232700','+48-71-344368','http://www.mercure.com/en/hotel-3374-hotel-mercure-wroclaw-centrum/index.shtml','16:00:00','12:00:00','h3374-re@accor.com','50-159',51.108700,17.039100,'',NULL,'2017-10-29 22:43:00'),(12,'radsonspb',16,7,'hotel',105,'+7-812-406-00-00',NULL,'https://www.radissonblu.com/en/sonyahotel-stpetersburg','15:00:00','12:00:00','sonya.sales@radissonblu.com','191187',59.946400,30.348000,'',NULL,'2017-10-29 22:58:00');
/*!40000 ALTER TABLE `gpt_hotel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_hotel_image_type`
--

DROP TABLE IF EXISTS `gpt_hotel_image_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel_image_type` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel_image_type`
--

LOCK TABLES `gpt_hotel_image_type` WRITE;
/*!40000 ALTER TABLE `gpt_hotel_image_type` DISABLE KEYS */;
INSERT INTO `gpt_hotel_image_type` VALUES (1,'exterior'),(2,'lobby'),(3,'guest room'),(4,'suite'),(5,'restaurant'),(6,'bar'),(7,'pool'),(8,'fitness center'),(9,'meeting room'),(10,'business center'),(11,'beach'),(12,'spa');
/*!40000 ALTER TABLE `gpt_hotel_image_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_hotel_images`
--

DROP TABLE IF EXISTS `gpt_hotel_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel_images` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hotel_id` bigint(20) NOT NULL,
  `type_id` tinyint(4) NOT NULL,
  `url` varchar(600) NOT NULL,
  `thumbnail_url` varchar(600) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`),
  KEY `type_id` (`type_id`),
  CONSTRAINT `gpt_hotel_images_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `gpt_hotel` (`id`),
  CONSTRAINT `gpt_hotel_images_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `gpt_hotel_image_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel_images`
--

LOCK TABLES `gpt_hotel_images` WRITE;
/*!40000 ALTER TABLE `gpt_hotel_images` DISABLE KEYS */;
INSERT INTO `gpt_hotel_images` VALUES (1,1,1,'http://www3.hilton.com/resources/media/hi/NYCNHHH/en_US/img/shared/full_page_image_gallery/main/HH_exteriornighttime01_1270x560_FitToBoxSmallDimension_LowerCenter.jpg','http://www3.hilton.com/resources/media/hi/NYCNHHH/en_US/img/shared/full_page_image_gallery/main/HH_exteriornighttime01_1270x560_FitToBoxSmallDimension_LowerCenter.jpg'),(2,1,3,'http://www3.hilton.com/resources/media/hi/NYCNHHH/en_US/img/shared/full_page_image_gallery/main/HH_execdoubles1_3_1270x560_FitToBoxSmallDimension_Center.jpg','http://www3.hilton.com/resources/media/hi/NYCNHHH/en_US/img/shared/full_page_image_gallery/main/HH_execdoubles1_3_1270x560_FitToBoxSmallDimension_Center.jpg'),(3,1,4,'http://www3.hilton.com/resources/media/hi/NYCNHHH/en_US/img/shared/full_page_image_gallery/main/HH_execpresidential01_13_1270x560_FitToBoxSmallDimension_Center.jpg','http://www3.hilton.com/resources/media/hi/NYCNHHH/en_US/img/shared/full_page_image_gallery/main/HH_execpresidential01_13_1270x560_FitToBoxSmallDimension_Center.jpg'),(4,1,8,'http://www3.hilton.com/resources/media/hi/NYCNHHH/en_US/img/shared/full_page_image_gallery/main/HH_fitrec_38_1270x560_FitToBoxSmallDimension_Center.jpg','http://www3.hilton.com/resources/media/hi/NYCNHHH/en_US/img/shared/full_page_image_gallery/main/HH_fitrec_38_1270x560_FitToBoxSmallDimension_Center.jpg'),(5,2,2,'http://www.starwoodhotels.com/pub/media/1598/she1598lo.174552_xx.jpg','http://www.starwoodhotels.com/pub/media/1598/she1598lo.174552_xx.jpg'),(6,2,3,'http://www.starwoodhotels.com/pub/media/1598/she1598gr.161580_xx.jpg','http://www.starwoodhotels.com/pub/media/1598/she1598gr.161580_xx.jpg'),(7,2,9,'http://www.starwoodhotels.com/pub/media/1598/she1598br.174546_xx.jpg','http://www.starwoodhotels.com/pub/media/1598/she1598br.174546_xx.jpg'),(8,2,10,'http://www.starwoodhotels.com/pub/media/1598/she1598sl.174542_xx.jpg','http://www.starwoodhotels.com/pub/media/1598/she1598sl.174542_xx.jpg'),(9,2,6,'http://www.starwoodhotels.com/pub/media/1598/she1598re.174551_xx.jpg','http://www.starwoodhotels.com/pub/media/1598/she1598re.174551_xx.jpg'),(10,3,1,'http://cache.marriott.com/propertyimages/m/miabb/phototour/miabb_phototour103.jpg?interpolation=progressive-bilinear&downsize=*:423px','http://cache.marriott.com/propertyimages/m/miabb/phototour/miabb_phototour103.jpg?interpolation=progressive-bilinear&downsize=*:423px'),(11,3,3,'http://cache.marriott.com/propertyimages/m/miabb/phototour/miabb_phototour131.jpg?interpolation=progressive-bilinear&downsize=*:423px','http://cache.marriott.com/propertyimages/m/miabb/phototour/miabb_phototour131.jpg?interpolation=progressive-bilinear&downsize=*:423px'),(12,3,7,'http://cache.marriott.com/propertyimages/m/miabb/phototour/miabb_phototour123.jpg?interpolation=progressive-bilinear&downsize=*:423px','http://cache.marriott.com/propertyimages/m/miabb/phototour/miabb_phototour123.jpg?interpolation=progressive-bilinear&downsize=*:423px'),(13,4,1,'http://doubletree3.hilton.com/resources/media/dt/MSQDTDI/en_US/img/shared/full_page_image_gallery/main/HL_facade002_2_677x380_FitToBoxSmallDimension_Center.jpg','http://doubletree3.hilton.com/resources/media/dt/MSQDTDI/en_US/img/shared/full_page_image_gallery/main/HL_facade002_2_677x380_FitToBoxSmallDimension_Center.jpg'),(14,4,3,'http://doubletree3.hilton.com/resources/media/dt/MSQDTDI/en_US/img/shared/full_page_image_gallery/main/HL_kroom_11_677x380_FitToBoxSmallDimension_Center.jpg','http://doubletree3.hilton.com/resources/media/dt/MSQDTDI/en_US/img/shared/full_page_image_gallery/main/HL_kroom_11_677x380_FitToBoxSmallDimension_Center.jpg'),(15,4,5,'http://doubletree3.hilton.com/resources/media/dt/MSQDTDI/en_US/img/shared/full_page_image_gallery/main/HL_ember011_9_677x380_FitToBoxSmallDimension_Center.jpg','http://doubletree3.hilton.com/resources/media/dt/MSQDTDI/en_US/img/shared/full_page_image_gallery/main/HL_ember011_9_677x380_FitToBoxSmallDimension_Center.jpg'),(16,4,9,'http://doubletree3.hilton.com/resources/media/dt/MSQDTDI/en_US/img/shared/full_page_image_gallery/main/HL_meeting012_16_677x380_FitToBoxSmallDimension_Center.jpg','http://doubletree3.hilton.com/resources/media/dt/MSQDTDI/en_US/img/shared/full_page_image_gallery/main/HL_meeting012_16_677x380_FitToBoxSmallDimension_Center.jpg'),(17,5,1,'http://www.yhotel.by/ru/pub/img/photos/37.jpg','http://www.yhotel.by/ru/pub/img/photos/37.jpg'),(18,5,4,'http://www.yhotel.by/ru/pub/suite_img/1409752733489104.jpg','http://www.yhotel.by/ru/pub/suite_img/1409752733489104.jpg'),(19,6,2,'/upload/resize_cache/iblock/d2e/1920_880_2/6_min.jpg','/upload/resize_cache/iblock/d2e/1920_880_2/6_min.jpg'),(20,6,12,'/upload/resize_cache/iblock/e3b/1920_880_2/im1_9127a.jpg','/upload/resize_cache/iblock/e3b/1920_880_2/im1_9127a.jpg'),(21,7,1,'http://cache.marriott.com/propertyimages/m/mhpbr/phototour/mhpbr_phototour01.jpg?interpolation=progressive-bilinear&downsize=*:423px','http://cache.marriott.com/propertyimages/m/mhpbr/phototour/mhpbr_phototour01.jpg?interpolation=progressive-bilinear&downsize=*:423px'),(22,7,3,'http://cache.marriott.com/propertyimages/m/mhpbr/phototour/mhpbr_phototour42.jpg?interpolation=progressive-bilinear&downsize=*:423px','http://cache.marriott.com/propertyimages/m/mhpbr/phototour/mhpbr_phototour42.jpg?interpolation=progressive-bilinear&downsize=*:423px'),(23,7,5,'http://cache.marriott.com/propertyimages/m/mhpbr/phototour/mhpbr_phototour16.jpg?interpolation=progressive-bilinear&downsize=*:423px','http://cache.marriott.com/propertyimages/m/mhpbr/phototour/mhpbr_phototour16.jpg?interpolation=progressive-bilinear&downsize=*:423px'),(24,7,7,'http://cache.marriott.com/propertyimages/m/mhpbr/phototour/mhpbr_phototour18.jpg?interpolation=progressive-bilinear&downsize=*:423px','http://cache.marriott.com/propertyimages/m/mhpbr/phototour/mhpbr_phototour18.jpg?interpolation=progressive-bilinear&downsize=*:423px'),(25,7,9,'http://cache.marriott.com/propertyimages/m/mhpbr/phototour/mhpbr_phototour22.jpg?interpolation=progressive-bilinear&downsize=*:423px','http://cache.marriott.com/propertyimages/m/mhpbr/phototour/mhpbr_phototour22.jpg?interpolation=progressive-bilinear&downsize=*:423px'),(26,8,3,'http://hostel-traveler.by/images/stories/2-ekonom/1.jpg','http://hostel-traveler.by/images/stories/2-ekonom/1.jpg'),(27,9,1,'http://www.hotel-neman.by/media/file/binary/2010/8/2/188000254132/01_jpg.jpg?srv=cms','http://www.hotel-neman.by/media/file/binary/2010/8/2/188000254132/01_jpg.jpg?srv=cms'),(28,9,3,'http://www.hotel-neman.by/media/file/binary/2010/10/25/180002415126/2mest_006_big_jpg.jpg?srv=cms','http://www.hotel-neman.by/media/file/binary/2010/10/25/180002415126/2mest_006_big_jpg.jpg?srv=cms'),(29,10,1,'http://www3.hilton.com/resources/media/hi/WAWHIHI/en_US/img/shared/full_page_image_gallery/main/HL_nexterior_2_1270x560_FitToBoxSmallDimension_Center.jpg','http://www3.hilton.com/resources/media/hi/WAWHIHI/en_US/img/shared/full_page_image_gallery/main/HL_nexterior_2_1270x560_FitToBoxSmallDimension_Center.jpg'),(30,10,4,'http://www3.hilton.com/resources/media/hi/WAWHIHI/en_US/img/shared/full_page_image_gallery/main/HL_roomex01_18_1270x560_FitToBoxSmallDimension_Center.jpg','http://www3.hilton.com/resources/media/hi/WAWHIHI/en_US/img/shared/full_page_image_gallery/main/HL_roomex01_18_1270x560_FitToBoxSmallDimension_Center.jpg'),(31,10,10,'http://www3.hilton.com/resources/media/hi/WAWHIHI/en_US/img/shared/full_page_image_gallery/main/hi_businesscenter05_2_1270x560_FitToBoxSmallDimension_Center.jpg','http://www3.hilton.com/resources/media/hi/WAWHIHI/en_US/img/shared/full_page_image_gallery/main/hi_businesscenter05_2_1270x560_FitToBoxSmallDimension_Center.jpg'),(32,10,12,'http://www3.hilton.com/resources/media/hi/WAWHIHI/en_US/img/shared/full_page_image_gallery/main/HL_fitnessclub08_3.jpg','http://www3.hilton.com/resources/media/hi/WAWHIHI/en_US/img/shared/full_page_image_gallery/main/HL_fitnessclub08_3.jpg'),(33,11,2,'http://www.ahstatic.com/photos/3374_ho_01_p_346x260.jpg','http://www.ahstatic.com/photos/3374_ho_01_p_346x260.jpg'),(34,11,3,'http://www.ahstatic.com/photos/3374_ro_00_p_346x260.jpg','http://www.ahstatic.com/photos/3374_ro_00_p_346x260.jpg'),(35,11,6,'http://www.ahstatic.com/photos/3374_ba_00_p_346x260.jpg','http://www.ahstatic.com/photos/3374_ba_00_p_346x260.jpg'),(36,11,9,'http://www.ahstatic.com/photos/3374_sm_01_p_346x260.jpg','http://www.ahstatic.com/photos/3374_sm_01_p_346x260.jpg'),(37,11,10,'http://www.ahstatic.com/photos/3374_bu_01_p_346x260.jpg','http://www.ahstatic.com/photos/3374_bu_01_p_346x260.jpg'),(38,12,4,'https://cache.carlsonhotels.com/galleries/radblu/photos/webextra/ledzs/home_marquee/LEDZS_rooms_1440x550.jpg','https://cache.carlsonhotels.com/galleries/radblu/photos/webextra/ledzs/home_marquee/LEDZS_rooms_1440x550.jpg'),(39,12,5,'https://cache.carlsonhotels.com/galleries/radblu/photos/webextra/ledzs/metamorfos_summary/metamorfos_268x238.jpg','https://cache.carlsonhotels.com/galleries/radblu/photos/webextra/ledzs/metamorfos_summary/metamorfos_268x238.jpg'),(40,12,8,'https://cache.carlsonhotels.com/galleries/radblu/photos/webextra/ledzs/health_fitness_summary/Fitness-Centre.jpg','https://cache.carlsonhotels.com/galleries/radblu/photos/webextra/ledzs/health_fitness_summary/Fitness-Centre.jpg'),(41,12,9,'https://cache.carlsonhotels.com/galleries/radblu/photos/webextra/ledzs/main_gallery/meetings_1_1280x960.jpg','https://cache.carlsonhotels.com/galleries/radblu/photos/webextra/ledzs/main_gallery/meetings_1_1280x960.jpg');
/*!40000 ALTER TABLE `gpt_hotel_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_hotel_name`
--

DROP TABLE IF EXISTS `gpt_hotel_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel_name` (
  `hotel_id` bigint(20) NOT NULL,
  `lang_id` tinyint(4) NOT NULL,
  `name` varchar(500) NOT NULL,
  KEY `hotel_id` (`hotel_id`),
  KEY `lang_id` (`lang_id`),
  CONSTRAINT `gpt_hotel_name_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `gpt_hotel` (`id`),
  CONSTRAINT `gpt_hotel_name_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `gpt_language` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel_name`
--

LOCK TABLES `gpt_hotel_name` WRITE;
/*!40000 ALTER TABLE `gpt_hotel_name` DISABLE KEYS */;
INSERT INTO `gpt_hotel_name` VALUES (1,2,'New York Hilton Midtown'),(2,2,'Sheraton Grand Los Angeles'),(3,2,'Miami Marriott Biscayne Bay'),(4,2,'DoubleTree by Hilton Hotel Minsk'),(5,2,'Hotel complex \"Yubileiny\"'),(5,1,'Гостиничный комплекс \"Юбилейный\"'),(6,2,'Victoria & Spa Hotel'),(6,1,'Отель Виктория & СПА'),(7,2,'Renaissance Minsk Hotel'),(8,2,'Trinity Hostel'),(8,1,'Хостел Тринити'),(9,2,'Hotel \"NEMAN\"'),(9,1,'Отель \"НЕМАН\"'),(10,2,'Hilton Warsaw Hotel and Convention Centre'),(11,2,'Hotel Mercure Wroclaw Centrum'),(12,2,'Radisson Sonya Hotel'),(12,1,'Отель Рэдиссон Соня');
/*!40000 ALTER TABLE `gpt_hotel_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_language`
--

DROP TABLE IF EXISTS `gpt_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_language` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) NOT NULL,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_language`
--

LOCK TABLES `gpt_language` WRITE;
/*!40000 ALTER TABLE `gpt_language` DISABLE KEYS */;
INSERT INTO `gpt_language` VALUES (1,'ru','Русский'),(2,'en','English');
/*!40000 ALTER TABLE `gpt_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_location`
--

DROP TABLE IF EXISTS `gpt_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_location` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `type_id` tinyint(4) NOT NULL,
  `parent` bigint(20) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `latitude` decimal(10,6) DEFAULT NULL,
  `longitude` decimal(10,6) DEFAULT NULL,
  `time_zone` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type_id` (`type_id`),
  CONSTRAINT `gpt_location_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `gpt_location_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_location`
--

LOCK TABLES `gpt_location` WRITE;
/*!40000 ALTER TABLE `gpt_location` DISABLE KEYS */;
INSERT INTO `gpt_location` VALUES (1,'NAM',1,NULL,'',NULL,NULL,NULL),(2,'EUR',1,NULL,'',NULL,NULL,NULL),(3,'US',2,1,'',NULL,NULL,NULL),(4,'CAN',2,1,'\0',NULL,NULL,NULL),(5,'BY',2,2,'',NULL,NULL,NULL),(6,'PL',2,2,'',NULL,NULL,NULL),(7,'RU',2,2,'',NULL,NULL,NULL),(8,'NY',3,3,'',NULL,NULL,NULL),(9,'LA',3,3,'',NULL,NULL,NULL),(10,'MI',3,3,'',NULL,NULL,NULL),(11,'TOR',3,4,'\0',NULL,NULL,NULL),(12,'MSQ',3,5,'',NULL,NULL,NULL),(13,'GRO',3,5,'\0',NULL,NULL,NULL),(14,'WAW',3,6,'',NULL,NULL,NULL),(15,'WRO',3,6,'',NULL,NULL,NULL),(16,'SPB',3,7,'',NULL,NULL,NULL);
/*!40000 ALTER TABLE `gpt_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_location_name`
--

DROP TABLE IF EXISTS `gpt_location_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_location_name` (
  `location_id` bigint(20) NOT NULL,
  `lang_id` tinyint(4) NOT NULL,
  `name` varchar(255) NOT NULL,
  KEY `location_id` (`location_id`),
  KEY `lang_id` (`lang_id`),
  CONSTRAINT `gpt_location_name_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `gpt_location` (`id`),
  CONSTRAINT `gpt_location_name_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `gpt_language` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_location_name`
--

LOCK TABLES `gpt_location_name` WRITE;
/*!40000 ALTER TABLE `gpt_location_name` DISABLE KEYS */;
INSERT INTO `gpt_location_name` VALUES (1,1,'Северная Америка'),(1,2,'North America'),(2,1,'Европа'),(2,2,'Europe'),(3,1,'США'),(3,2,'USA'),(4,1,'Канада'),(4,2,'Canada'),(5,1,'Беларусь'),(5,2,'Belarus'),(6,1,'Польша'),(6,2,'Poland'),(7,1,'Россия'),(7,2,'Russia'),(8,1,'Нью-Йорк'),(8,2,'New York'),(9,1,'Лос-Анджелес'),(9,2,'Los Angeles'),(10,1,'Майами'),(10,2,'Miami'),(11,1,'Торонто'),(11,2,'Toronto'),(12,1,'Минск'),(12,2,'Minsk'),(13,1,'Гродно'),(13,2,'Grodno'),(14,1,'Варшава'),(14,2,'Warsaw'),(15,1,'Вроцлав'),(15,2,'Wroclaw'),(16,1,'Санкт-Петербург'),(16,2,'Saint-Petersberg');
/*!40000 ALTER TABLE `gpt_location_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_location_type`
--

DROP TABLE IF EXISTS `gpt_location_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_location_type` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_location_type`
--

LOCK TABLES `gpt_location_type` WRITE;
/*!40000 ALTER TABLE `gpt_location_type` DISABLE KEYS */;
INSERT INTO `gpt_location_type` VALUES (1,'continent'),(2,'country'),(3,'city');
/*!40000 ALTER TABLE `gpt_location_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-31  2:25:46
