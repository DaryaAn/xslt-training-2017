-- 1) подсчитать количество отелей в городе "Минск"
select count(*)
  from gpt_hotel
  join gpt_location on (gpt_hotel.city_id=gpt_location.id)
  join gpt_location_name on (gpt_location.id=gpt_location_name.location_id)
 where gpt_location_name.name='Минск';

-- 2) Вывести имена активных отелей, содержащих слово "Hilton"
select name 
  from gpt_hotel_name 
  join gpt_hotel on (gpt_hotel_name.hotel_id=gpt_hotel.id) 
 where active=1 
   and name like '%Hilton%';
   
/* 2) Вывести активные отели, находящиеся в городе "Минск", имя отеля должно быть выведено на английском языке. Отобразить следующую информацию:
- имя города
- имя отеля
- телефон
- почту
- долготу
- широту
*/

select ln.name as city_name
	  ,hn.name as hotel_name
	  ,h.phone
      ,h.email
      ,h.longitude
      ,h.latitude
  from gpt_hotel h
  join gpt_location l on (h.city_id=l.id)
  join gpt_location_name ln on (l.id=ln.location_id)
  join gpt_hotel_name hn on (h.id=hn.hotel_id)
  join gpt_language lg on (hn.lang_id=lg.id)
 where ln.name='Минск'
   and h.active=1  
   and lg.name='English';

-- 3) Вывести имена отелей, у которых количество картинок большех трёх

select hn.name
  from gpt_hotel h
  join gpt_hotel_name hn on (h.id=hn.hotel_id )
  join gpt_language lg on (hn.lang_id=lg.id and lg.name='English')
  join gpt_hotel_images hi on (h.id=hi.hotel_id)
 group by hn.name 
having count(hi.id) > 3;

-- 4) Вывести в одну строку через запятую имя отеля, его аддрес, тип и город в котором он находится

select concat(hn.name, ', ', h.zip, ', ', h.category_id, ', ', hn.name) as hotel_text
  from gpt_hotel h
  join gpt_hotel_name hn on (h.id=hn.hotel_id )
  join gpt_location l on (h.city_id=l.id)
  join gpt_location_name ln on (l.id=ln.location_id)
  join gpt_language lgh on (hn.lang_id=lgh.id and lgh.name='English')
  join gpt_language lgl on (ln.lang_id=lgl.id and lgl.name='English');
  

-- 5) Вывести максимальный ID из таблицы GPT_HOTEL_IMAGES

select max(id) from gpt_hotel_images;

-- 6) Вывести имена всех стран на русском языке

select ln.name as country_name
  from gpt_location_name ln
  join gpt_location l on (ln.location_id=l.id)
  join gpt_location_type lt on (l.type_id=lt.id and lt.type_name='country')
  join gpt_language lg on (ln.lang_id=lg.id and lg.name='Русский');
  
