<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0" xmlns:h="HouseChema" xmlns:i="HouseInfo" xmlns:r="RoomsChema">
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
    
    <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
    <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
    
    <xsl:template match="/">
        <AllRooms>
            
            <xsl:for-each select="//r:Room">
                <xsl:sort select="./ancestor::h:House/@City"/>
                <xsl:sort select="./ancestor::h:Block/@number"/>
                <xsl:sort select="@nuber" order="ascending" data-type="number"/>
             <Room>
             <Address>
                 <xsl:value-of select="concat(translate(./ancestor::h:House/@City, $smallcase, $uppercase), '/', ./ancestor::h:House/i:Address,'/',./ancestor::h:Block/@number,'/',@nuber)"/> 
             </Address>
                 
             <HouseRoomsCount>
                 <xsl:value-of select="count(./ancestor::h:House//r:Room)"/>
             </HouseRoomsCount>
             
             <BlockRoomsCount>
                 <xsl:value-of select="count(./ancestor::h:Block//r:Room)"/>
             </BlockRoomsCount>
                 
             <HouseGuestsCount>
                 <xsl:value-of select="sum(./ancestor::h:House//@guests)"/>
             </HouseGuestsCount>
             
             <GuestsPerRoomAverage>
                 <xsl:value-of select="floor(sum(./ancestor::h:House//@guests) div count(./ancestor::h:House//r:Room))"/>
             </GuestsPerRoomAverage>                 
                
            <xsl:choose>
                <xsl:when test="@guests=1">
                    <Allocated Single="true" Double="false" Triple="false" Quarter="false"/>
                </xsl:when>
                <xsl:when test="@guests=2">
                    <Allocated Single="false" Double="true" Triple="false" Quarter="false"/>
                </xsl:when>
                <xsl:when test="@guests=3">
                    <Allocated Single="false" Double="false" Triple="true" Quarter="false"/>
                </xsl:when>
                <xsl:when test="@guests=4">
                    <Allocated Single="false" Double="false" Triple="false" Quarter="true"/>
                </xsl:when>
            </xsl:choose>
             </Room>        
        </xsl:for-each>
            
                </AllRooms>
    </xsl:template>
    
</xsl:stylesheet>