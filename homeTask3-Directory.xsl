<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
    
    <xsl:key name="names_list" match="item" use="substring(@Name, 1, 1)"/>
    
    <xsl:template match="list">
        <list>
            <xsl:for-each select="item[count(. | key('names_list', substring(@Name, 1, 1))[1]) = 1]">
                <xsl:sort select="@Name" />
                <capital value="{substring(@Name, 1, 1)}">
                    <xsl:for-each select="key('names_list', substring(@Name, 1, 1))">
                        <xsl:sort select="@Name" />
                        <name><xsl:value-of select="@Name" /></name>
                    </xsl:for-each>
                </capital>
            </xsl:for-each>
        </list>
    </xsl:template>
      
</xsl:stylesheet>
