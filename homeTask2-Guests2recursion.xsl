<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
    
    <xsl:template name="split_string">
        <xsl:param name="data_string"/>
        <xsl:param name="delimiter" />
        <xsl:choose>
            <xsl:when test="contains($data_string, $delimiter)">
                <Guest Age="{substring-before(substring-after($data_string, 'Age:'), '|')}"
                    Nationality="{substring-before(substring-after($data_string, 'Nationality:'), '|')}" 
                    Gender="{substring-before(substring-after($data_string, 'Gender:'), '|')}" 
                    Name="{substring-before(substring-after($data_string, 'Name:'), '|')}">
                    <Type><xsl:value-of select="substring-before(substring-after($data_string, 'Type:'), '|')"/></Type>
                    <Profile>
                        <Address><xsl:value-of select="substring-before(substring-after($data_string, 'Address:'), '#')"/></Address>
                    </Profile>  
                </Guest>
                <xsl:call-template name="split_string">
                    <xsl:with-param name="data_string">
                        <xsl:value-of select="substring-after($data_string, $delimiter)"/>
                    </xsl:with-param>
                    <xsl:with-param name="delimiter">
                        <xsl:value-of select="$delimiter"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="data">
        <Guests>
            <xsl:call-template name="split_string">
                <xsl:with-param name="data_string">
                    <xsl:value-of select="."/>
                </xsl:with-param>
                <xsl:with-param name="delimiter">#</xsl:with-param>
            </xsl:call-template>
        </Guests>
    </xsl:template>
    
</xsl:stylesheet>