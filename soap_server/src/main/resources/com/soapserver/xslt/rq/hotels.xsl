<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:ent="http://soapserver.com/entities"
	xmlns:requtil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.RequestUtil"
	exclude-result-prefixes="ent requtil">
	
	<xsl:output method="text"/>
	
	<xsl:template match="/">
		<xsl:variable name="lang" select="ent:HotelsRequest/ent:Lang"/>
		<xsl:variable name="countryCode" select="ent:HotelsRequest/ent:CountryCode"/>
		<xsl:variable name="cityName" select="ent:HotelsRequest/ent:CityName"/>
		<xsl:variable name="hotelName" select="ent:HotelsRequest/ent:HotelName"/>
		<xsl:variable name="hotelCategory" select="ent:HotelsRequest/ent:HotelCategory"/>
		
		<xsl:choose>
			<xsl:when test="string-length($countryCode) = 0">
				<xsl:text>Parameter Country Code is Empty</xsl:text>
				<xsl:message>
					<xsl:value-of select="requtil:overrideResponse()"/>
				</xsl:message>
			</xsl:when>

			<xsl:when test="string-length($lang) = 0">
				<xsl:text>Parameter Lang is Invalid</xsl:text>
				<xsl:message>
					<xsl:value-of select="requtil:overrideResponse()"/>
				</xsl:message>
			</xsl:when>

			<xsl:otherwise>
				<xsl:text>select coun.code as country_code,
		 coun_n.name as country_name,
		 city.code as city_code,
		 city_n.name as city_name,
		 hotel.code	as hotel_code,
		 coalesce(hotel_n.name, hotel_ne.name, '(no_name)') as hotel_name,
		 hotel.phone as contact_phone,
		 hotel.fax as contact_fax,
		 hotel.email as contact_email,
		 hotel.url as contact_url,
		 hotel.check_in,
		 hotel.check_out,
		 concat(hotel.longitude, ' ', hotel.latitude) as coordinates
  from gpt.gpt_location coun
 inner join gpt.gpt_location_type coun_t on (coun.type_id = coun_t.id and coun_t.type_name = 'country')
 inner join gpt.gpt_location_name coun_n on (coun.id = coun_n.location_id and coun_n.lang_id = (select id from gpt.gpt_language where code = '</xsl:text><xsl:value-of select="$lang"/><xsl:text>'))
 inner join gpt.gpt_location city on (city.parent = coun.id)
 inner join gpt.gpt_location_name city_n on (city.id = city_n.location_id and city_n.lang_id = (select id from gpt.gpt_language where code = '</xsl:text><xsl:value-of select="$lang"/><xsl:text>'))
 inner join gpt.gpt_hotel hotel on (hotel.city_id = city.id)
  left join gpt.gpt_hotel_name hotel_n on (hotel.id = hotel_n.hotel_id and hotel_n.lang_id = (select id from gpt.gpt_language where code = '</xsl:text><xsl:value-of select="$lang"/><xsl:text>'))
  left join gpt.gpt_hotel_name hotel_ne on (hotel.id = hotel_ne.hotel_id and hotel_ne.lang_id = (select id from gpt.gpt_language where code = 'en'))
 where coun.code = '</xsl:text><xsl:value-of select="$countryCode"/><xsl:text>'</xsl:text>
				<xsl:if test="string-length($cityName) &gt; 0">
					<xsl:text> and city_n.name="</xsl:text><xsl:value-of select="$cityName"/><xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="string-length($hotelName) &gt; 0">
					<xsl:text> and hotel_n.name like "%</xsl:text><xsl:value-of select="$hotelName"/><xsl:text>%"</xsl:text>
				</xsl:if>
				<xsl:if test="string-length($hotelCategory) &gt; 0">
					<xsl:text> and hotel.category_id = "</xsl:text><xsl:value-of select="$hotelCategory"/><xsl:text>"</xsl:text>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
</xsl:stylesheet>