<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:ent="http://soapserver.com/entities"
	xmlns:sutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.StringUtil"
	xmlns:queryutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.SqlQueriesUtil"
	exclude-result-prefixes="ent sutil queryutil">
	
	<xsl:output method="xml"/>
	
	<xsl:template match="/">
		
		<ent:HotelsResponse>
			<ent:Hotels>
			<xsl:for-each select="Result/Entry">
				<xsl:variable name="hotelName" select="hotel_name"/>
				<xsl:variable name="hotelCode" select="hotel_code"/>			
					<ent:Hotel Name="{$hotelName}" Code="{$hotelCode}">
						<ent:CountryName>
							<xsl:value-of select="country_name"/>
						</ent:CountryName>
						<ent:CountryCode>
							<xsl:value-of select="country_code"/>
						</ent:CountryCode>
						<ent:CityName>
							<xsl:value-of select="city_name"/>
						</ent:CityName>
						<ent:CityCode>
							<xsl:value-of select="city_code"/>
						</ent:CityCode>
						<ent:Phone>
							<xsl:value-of select="contact_phone"/>
						</ent:Phone>
						<ent:Fax>
							<xsl:value-of select="contact_fax"/>
						</ent:Fax>
						<ent:Email>
							<xsl:value-of select="contact_email"/>
						</ent:Email>
						<ent:Url>
							<xsl:value-of select="contact_url"/>
						</ent:Url>
						<ent:CheckIn>
							<xsl:value-of select="check_in"/>
						</ent:CheckIn>
						<ent:CheckOut>
							<xsl:value-of select="check_out"/>
						</ent:CheckOut>
						<ent:Coodinates>
							<xsl:value-of select="coordinates"/>
						</ent:Coodinates>
					</ent:Hotel>
				</xsl:for-each>
			</ent:Hotels>
		</ent:HotelsResponse>
	</xsl:template>
	
</xsl:stylesheet>