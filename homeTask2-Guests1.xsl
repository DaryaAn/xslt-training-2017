<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    
    <xsl:template match="/">
        <xsl:for-each select="//Guest">
            <xsl:value-of select="concat('Age:',@Age, '|','Nationality:',@Nationalty, '|','Gender:',@Gender, '|','Name:', @Name, '|', 'Type:',./Type, '|', 'Address:', //Address)"/>
            <xsl:text>#</xsl:text>
        </xsl:for-each>
    </xsl:template>
    
</xsl:stylesheet>