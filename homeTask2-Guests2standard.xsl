<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
    
    <xsl:template match="/">
        <Guests>
            <Guest Age="{substring-before(substring-after(., 'Age:'), '|')}"
                Nationality="{substring-before(substring-after(., 'Nationality:'), '|')}" 
                Gender="{substring-before(substring-after(., 'Gender:'), '|')}" 
                Name="{substring-before(substring-after(., 'Name:'), '|')}">
                <Type><xsl:value-of select="substring-before(substring-after(., 'Type:'), '|')"/></Type>
                <Profile>
                    <Address><xsl:value-of select="substring-before(substring-after(., 'Address:'), '#')"/></Address>
                </Profile>  
            </Guest>
            <Guest Age="{substring-before(substring-after(substring-after(., '#'), 'Age:'), '|')}"
                Nationality="{substring-before(substring-after(substring-after(., '#'), 'Nationality:'), '|')}" 
                Gender="{substring-before(substring-after(substring-after(., '#'), 'Gender:'), '|')}" 
                Name="{substring-before(substring-after(substring-after(., '#'), 'Name:'), '|')}">
                <Type><xsl:value-of select="substring-before(substring-after(substring-after(., '#'), 'Type:'), '|')"/></Type>
                <Profile>
                    <Address><xsl:value-of select="substring-before(substring-after(substring-after(., '#'), 'Address:'), '#')"/></Address>
                </Profile>
            </Guest>
            <Guest Age="{substring-before(substring-after(substring-after(substring-after(., '#'), 'Address:'), '#Age:'), '|')}"
                Nationality="{substring-before(substring-after(substring-after(substring-after(., '#'), 'Address:'), 'Nationality:'), '|')}" 
                Gender="{substring-before(substring-after(substring-after(substring-after(., '#'), 'Address:'), 'Gender:'), '|')}" 
                Name="{substring-before(substring-after(substring-after(substring-after(., '#'), 'Address:'), 'Name:'), '|')}">
                <Type><xsl:value-of select="substring-before(substring-after(substring-after(substring-after(., '#'), 'Address:'), 'Type:'), '|')"/></Type>
                <Profile>
                    <Address><xsl:value-of select="substring-before(substring-after(substring-after(substring-after(., '#'), 'Address:'), 'Address:'), '#')"/></Address>
                </Profile>
            </Guest>
        </Guests>
    </xsl:template>    
</xsl:stylesheet>