<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output method="xml" encoding="UTF-8"/>
    
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>
    
   
    <xsl:template match="Total/@AmountAfterTax">
        <xsl:attribute name="AmountAfterTax">
            <xsl:value-of select="round(.)"/>
        </xsl:attribute>
    </xsl:template>    
 
    <xsl:template match="RoomRate">
        <Offer>
            <xsl:apply-templates select="node()|@*"/>
        </Offer>
    </xsl:template>
    
    <xsl:template match="RoomRates">
        <Offers>
            <xsl:apply-templates/>
        </Offers>
    </xsl:template>
    
    <xsl:template match="RoomStay">
        <Hotel>
            <xsl:apply-templates select="RoomRates|node()|@*"/>
        </Hotel>
    </xsl:template>
    
    <xsl:template match="RoomStays">
        <Hotels>
            <xsl:apply-templates/>
        </Hotels>
    </xsl:template>


    
</xsl:stylesheet>